'''import libraries that will be used'''
import uuid

#Creates unique name
unique_name = uuid.uuid4()
#Creates raxml path
raxml_path = f'/Project/results/{unique_name}'

rule all:
    input:
        f'/Project/results/mafft_result_{unique_name}.phylip',
        f'{raxml_path}/mafft_result_{unique_name}.phylip'
    message:
        'Files and folders created sucessfully.'

#Align sequences from path
rule sequences_alignment:
    input:
        '/Project/sequences'
    output:
        f'/Project/results/mafft_result_{unique_name}.phylip'
    shell:
        'python ./scripts/sequences_alignment.py {input} {output}'
        ' --localpair --maxiterate 1000 --phylipout --inputorder'

#Creates phylogenetic tree from mafft file
rule phylogenetic_tree:
    input:
        f'/Project/results/mafft_result_{unique_name}.phylip',
    output:
        f'{raxml_path}/mafft_result_{unique_name}.phylip'
    shell:
        '''
        mkdir -p {raxml_path}
        python ./scripts/phylogenetic_tree.py {input} {raxml_path} --model LG+G7+F --tree pars{{10}} --bs-trees 100
        '''
        
