'''Import libraries that will be used'''
import os
import subprocess
import sys
import tempfile

def load_files(sequences_path):
    '''
    Load files present in the path.

    Args:
        sequences_path: location of the sequences files.

    Raises:
        ValueError: Path is empty.
        TypeError: File extension is not accepted.

    Returns:
        sequences_files (list): returns a list with the files from path.
    '''

    #Creates a list with the files from path
    sequences_files = os.listdir(sequences_path)
    #Creates a list with extensions
    accepted_extensions = ['.faa', '.fasta', '.fna']
    
    #Checks if list is empty
    if not sequences_files:
        #Raise ValueError if list is empty
        raise ValueError(f'Directory {sequences_path} is empty.')

    #For each file in path
    for file in sequences_files:
        #Separate file name from extension
        file_name, file_extension = os.path.splitext(file)
        #Force extension name to lowercase
        file_extension = file_extension.lower()
        #Checks if extension is in extensions list
        if not file_extension in accepted_extensions:
            #Raise TypeError if file extension in not an accepted extension
            raise TypeError(f'File {file} extension is of wrong type.')
    
    #Message appears if all files are loaded sucessfully
    print(f'Files from {sequences_path} loaded successfully.')
    #Returns file list
    return sequences_files

def concatenate_files(sequences_files, sequences_path):
    '''
    Concatenates the sequences in the files list in one temporary file.

    Args:
        sequences_files(list): files list.
        sequences_path: location of the sequences files.

    Returns:
        temp_file: returns a temporary FASTA file containing the sequences.
    '''

    #Creates temporary file in path
    temp_file = tempfile.NamedTemporaryFile(mode = 'w+',
                                            prefix = 'Concatenate_',
                                            suffix = '.fasta', 
                                            dir = sequences_path)
    #Creates variable with temporary file path
    temp_file_name = temp_file.name

    #For each file in file list
    for file in sequences_files:
        #Open file for reading
        with open(f'{sequences_path}/{file}', 'r') as sequence_file:
            file_content = sequence_file.read()
            #For each character in list
            for character in [' ', ';', ':', ',', '(', ')',
                              '[', ']', '\'', '\"']:
                #Checks if character is in file
                if character in file_content:
                    #Replaces character in file content
                    file_content = file_content.replace(character, '')
            #write file sequences in temporary file
            temp_file.write(f'{file_content}\n')
    #Rewinds file pointer to the beginning
    temp_file.seek(0)
    #Message appears after files are concatenated
    print(f'Files from {sequences_path} concatenated successfully.')
    #Returns temporary file and its path
    return temp_file, temp_file_name

def mafft_align(temp_file, temp_file_name, result_file, mafft_method):
    '''
    Aligns the sequences in the temporary file using mafft and saves them
    in a fasta file.

    Args:
        temp_file(list): temporary file with sequences.
        temp_file_name: temporary file path.
        result_path: location of the results files.
        mafft_method: method used in mafft.
    '''

    #Creates variable with mafft command
    mafft_command = (f'/opt/conda/bin/mafft {mafft_method}' 
                     f' {temp_file_name} > {result_file}')
    
    #Running mafft command
    mafft_result = subprocess.run([mafft_command], shell=True)

    #Closing temporary file
    temp_file.close()
    
    #Message appears after file is mafft aligned
    print(f'Command runned: {mafft_command}')
    print(f'Mafft alignment sucessfully.\nResult generated: {result_file}')

#Main
def main(sequences_path, result_file, mafft_method, sequences_files):
    '''All program steps will be executed.'''
    temp_file, temp_file_name = concatenate_files(sequences_files, sequences_path)
    mafft_align(temp_file, temp_file_name, result_file, mafft_method)
    
#This block only runs when the python file is called directly
if __name__ == "__main__":
    #Reads sequence path, result file and mafft method
    try:
        #Indicates where the sequences for alignment are located
        sequences_path = sys.argv[1]
        #Indicates the path and name of the mafft file
        result_file = sys.argv[2]
        #Indicates mafft method 
        mafft_method = ' '.join(sys.argv[3:])

        sequences_files = load_files(sequences_path)
        main(sequences_path, result_file, mafft_method, sequences_files)
    except IndexError:
        #Prints missing directory or output file error message
        print('Choose directory and output file name.')
        #Exit program with error
        sys.exit(1)
    except FileNotFoundError:
        #Prints wrong directory error message
        print('Choose an existing directory.')
        #Exit program with error
        sys.exit(1)
    

