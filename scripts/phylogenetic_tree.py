'''Import libraries that will be used'''
import os
import subprocess
import sys

def load_file(aligned_file):
    '''
    Load file present in the path.

    Args:
        aligned_file: path and name of the aligned file.

    Raises:
        TypeError: File extension is not accepted. 

    Returns:
        aligned_file: returns aligned file.
    '''

    #Creates a list with extensions
    accepted_extensions = ['.fasta', '.phylip']
    #Separate file name from extension
    file_name, file_extension = os.path.splitext(aligned_file)
    #Force extension name to lowercase
    file_extension = file_extension.lower()

    #Checks if extension is in extensions list
    if not file_extension in accepted_extensions:
        #Raise TypeError if file extension in not an accepted extension
        raise TypeError(f'File {aligned_file} extension is of wrong type.')
    #Returns aligned file
    return aligned_file

def move_file(aligned_file, raxml_path):
    '''
    Moves aligned file to raxml path
    
    Args:
        aligned_file: path and name of the aligned file.
        raxml_path: path and name of the folder.
    Raises:
        OSError: Cannot create a file.
    Returns:
        new_aligned_file: returns the new aligned file path.
    '''

    #Splits aligned file name and path in variable
    file_format = os.path.split(aligned_file)
    #Creates new aligned path
    new_aligned_file = f'{raxml_path}/{file_format[1]}'
    #Moves aligned file to new path
    os.rename(aligned_file, new_aligned_file)
    
    #Message appears after aligned file is moved to new path
    print(f'File: {file_format[1]} sucessfully moved to {raxml_path}')
    #Returns the new aligned file path
    return new_aligned_file, file_format[1]

def raxml_tree(new_aligned_file, raxml_method, raxml_path):
    '''
    Creates the raxml files for the phylogenetic tree using raxml-ng.

    Args:
        new_aligned_file: new aligned file path.
        raxml_method: method used in raxml.
    Returns:
        raxml_method_string (string): returns string containing raxml 
        method.
    '''

    #Converts raxml_method list into string
    raxml_method_string = ' '.join(raxml_method)
    #Creates variable with mafft command
    raxml_command = (f'raxml-ng --all --msa {new_aligned_file}'
                    f' {raxml_method_string}')
    
    #Running raxml command
    raxml_result = subprocess.run([raxml_command], shell=True)
    
    #Message appears after raxml trees are created 
    print(f'Command runned: {raxml_command}')
    print('Raxml-ng run completed.')
    #Returns string containing raxml method
    return raxml_method_string

def raxml_rerun(raxml_method_string, raxml_reduced_file):
    '''
    Runs raxml-ng with raxml.reduced.

    Args:
        raxml_method_string: string containing method used in raxml.
        raxml_reduced_file: raxml.reduced file.
    '''

    #Creates variable with mafft command
    raxml_command = (f'raxml-ng --all --msa {raxml_reduced_file}'
                    f' {raxml_method_string}')

    #Running raxml command
    raxml_result = subprocess.run([raxml_command], shell=True)
    
    #Message appears after raxml trees are created 
    print(f'Command runned: {raxml_command}')
    print('Raxml-ng rerun completed.')

#Main
def main(aligned_file, raxml_path, raxml_method):
    '''All program steps will be executed.'''
    new_aligned_file, file_name = move_file(aligned_file, 
                                            raxml_path)
    raxml_method_string = raxml_tree(new_aligned_file, raxml_method,
                                     raxml_path)
    #Creates variable with possible raxml.reduced file
    raxml_reduced_file = f'{raxml_path}/{file_name}.raxml.reduced.phy'
    #Checks if raxml.reduced file exists and runs raxml again
    if os.path.isfile(raxml_reduced_file):
        raxml_rerun(raxml_method_string, raxml_reduced_file)           
    
#This block only runs when the python file is called directly
if __name__ == "__main__":
    #Reads aligned file, result file and raxml method
    try:
        #Indicates the aligned file
        aligned_file = sys.argv[1]
        #Indicates the raxml folder path
        raxml_path = sys.argv[2]
        #Indicates raxml method
        raxml_method = sys.argv[3:]

        load_file(aligned_file)
        main(aligned_file, raxml_path, raxml_method)
    except IndexError:
        #Exit program with error
        print('Choose file and directory.')
        sys.exit(1)
    except FileNotFoundError:
        print('Choose an existing file.')
        #Exit program with error
        sys.exit(1)
    



