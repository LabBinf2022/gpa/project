FROM snakemake/snakemake:v7.18.2

LABEL version="1.0.0"
LABEL description="Imagem do snakemake para o projeto de BioLab"
LABEL maintainer="Samuel.Ricardo.Correia87@gmail.com"

WORKDIR /Project/
COPY scripts/ /Project/scripts/
COPY tests/ /Project/tests/
COPY Snakefile /Project/

RUN conda config --add channels defaults &&\
    conda config --add channels bioconda &&\
    conda config --add channels conda-forge &&\
    conda config --set channel_priority strict

RUN conda update conda
RUN conda install mamba
RUN mamba install mafft=7.508 -y
RUN mamba install raxml-ng=1.1.0 -y

CMD ["/bin/bash"]

