﻿# Bioinformatics Laboratory

###  "Comparative functional genomics and molecular phylogeny of Lactobacillus acidophilus strains and automatization."

In this project we will perform comparative and functional genomics followed by a phylogenetic study of the 16S protein of different strains of  the bacteria *Lactobacillus Acidophilus*.
 To do this we will use the platform https://www.bv-brc.org/ for genomic analysis and in linux shell command line for the phylogenetic analysis. The first one we will just need the genomes of each strain and for the second we will need to get the 16S sequences of each strain, align them using MAFFT v7.490, afterwards apply RaXML-NG v1.1.0 for Maximum-Likelihood algorithm and to finish FigTree v1.4.4 to get the image of the tree with bootstrap values out of the raxml file. MAFFT, RaXML-NG and Figtree portions are all automated, we only require the user to feed in the unaligned sequences file.

## Methodology
#### Genomics
1. Use the tool Proteome Comparison  (B) in the platform bv.brc.org 
2. Select reference and sequences
3. Wait for the circus file
#### Phylogeny
4. Gather your sequences into one file
5. Use a file format accepted by MAFFT to apply the alignment
6. Apply RaXML-NG with the right model
7. Apply the .support file in FigTree
8. Compare the results

Compare the results

## Requirements

Docker installation

## Starting

Pull docker image:

`docker pull samuelrc87/gpa:latest`

Run docker image with volumes:
`docker run -v $HOME/GPA/Inputs/:/Project/sequences -v $HOME/GPA/Outputs/:/Project/results -i -t samuelrc87/gpa:latest`

**Note**: `$HOME/GPA/Inputs` is the path to the directory that you want to use.  
`/Project/sequences` is the path where the directory is mounted in the container.  
The generated outputs will be placed in `/GPA/Outputs` directory.  

Execute Snakemake using all cores:  
`snakemake --cores all`

If you don't want to get inside the docker, you can use the following command: 
`docker run -v $HOME/GPA/Inputs/:/Project/sequences -v $HOME/GPA/Outputs/:/Project/results -i -t samuelrc87/gpa:latest snakemake --cores all`


