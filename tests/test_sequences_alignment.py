'''Import libraries that will be used'''
import unittest
import tempfile
from scripts.sequences_alignment import concatenate_files, load_files, mafft_align

#test path: indicates test data location
TEST_PATH = '/workspaces/Project/tests/test_files'
#result path: indicates result data location
RESULT_PATH = '/workspaces/Project/tests/result_files'

class TestConcatenate(unittest.TestCase):
    
    def test_load_files(self):
        '''Test load file'''
        #Creates the list with file names
        test_result = ['Lactobacillus acidophilus La-14 16S rsmG.fna',
                       'Lactobacillus acidophilus La-14 16S rsmH.fna']
        #Creates result
        result = load_files(f'{TEST_PATH}/test_sequences')
        
        #Compares the result with the test results
        self.assertEqual(result, test_result)
    
    def test_load_files_empty(self):
        '''Test load file empty path exception'''
        #Checks if exception is thrown
        with self.assertRaises(ValueError):
            load_files(f'{TEST_PATH}/test_empty')
    
    def test_load_files_extension(self):
        '''Test load file wrong extension exception'''
        #Checks if exception is thrown
        with self.assertRaises(TypeError):
            load_files(f'{TEST_PATH}/test_wrong_extension')
    
    def test_concatenate_files(self):
        '''Test concatenate files'''
        #Creates the list with file names
        test_data = ['Lactobacillus acidophilus La-14 16S rsmG.fna',
                     'Lactobacillus acidophilus La-14 16S rsmH.fna']
        #Creates the test result
        test_result = open((f'{RESULT_PATH}/Concatenate sequences.fasta'))
        #Creates the result
        result, result_name = concatenate_files(test_data,f'{TEST_PATH}/test_sequences')
        
        #Compares the result content with the test result content
        self.assertEqual(result.read(), test_result.read())
        
        #Close the result
        result.close()
        #Close the test result
        test_result.close()

    
    def test_mafft_align(self):
        '''Test mafft align'''
        #Creates the test data
        test_data = (f'{TEST_PATH}/Concatenate_sequences.fasta' )
        #Creates the test result
        test_result = open(f'{RESULT_PATH}/Mafft result.fasta')
        #Creates the test temp file
        test_temp_file = tempfile.TemporaryFile()
        #Runs mafft align
        mafft_align(test_temp_file,test_data,
                    f'{TEST_PATH}/mafft_result.fasta',
                    '--retree 1 --maxiterate 0 --inputorder')
        #Creates result
        result = open(f'{TEST_PATH}/mafft_result.fasta')

        #Compares the result content with the test result content
        self.assertEqual(result.read(), test_result.read())

        #Closes the result
        result.close()
        #Closes the test result
        test_result.close()

#This block only runs when the python file is called directly
if __name__ == '__main__':
    unittest.main()
