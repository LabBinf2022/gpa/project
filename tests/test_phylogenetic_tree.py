'''Import libraries that will be used'''
import unittest
import tempfile
from scripts.phylogenetic_tree import load_file, move_file, raxml_tree, raxml_rerun

#test path: indicates test data location
TEST_PATH = '/workspaces/Project/tests/test_files'
#result path: indicates result data location
RESULT_PATH = '/workspaces/Project/tests/result_files'

class TestConcatenate(unittest.TestCase):
    
    def test_load_file(self):
        '''Test load file'''
        #Creates file path and name
        test_result = f'{TEST_PATH}/mafft_result.fasta'
        #Creates result
        result = load_file(f'{TEST_PATH}/mafft_result.fasta')
        
        #Compares the result with the test results
        self.assertEqual(result, test_result)
     
    def test_load_file_extension(self):
        '''Test load file wrong extension exception'''
        #Checks if exception is thrown
        with self.assertRaises(TypeError):
            load_file(f'{TEST_PATH}/test_wrong_extension')
    
#This block only runs when the python file is called directly
if __name__ == '__main__':
    unittest.main()
